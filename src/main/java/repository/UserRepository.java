package repository;

import domain.User;
import utility.InputUtility;
import utility.JpaHelper;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;

public class UserRepository {

    JpaHelper jpaHelper = new JpaHelper();

    public <T> List<T> executeTypedQuery(String query, Class<T> type) {
        return jpaHelper.executeWithTx(em -> {
            TypedQuery<T> typedQuery = em.createQuery(query, type);
            return typedQuery.getResultList();
        });
    }

    public User executeTypedQuerySingle(String query) {
       return jpaHelper.executeWithTx(em -> {
            TypedQuery<User> typedQuery = em.createQuery(query, User.class);
            return typedQuery.getSingleResult();
        });
    }

    public <T> T executeTypedQuerySingle(String query, Class<T> typeCOfClass) {
        return jpaHelper.execute(em -> {
            TypedQuery<T> typedQuery = em.createQuery(query, typeCOfClass);
            return typedQuery.getSingleResult();
        });

    }

    public <T> T getUserById(long id, Class<T> tClass) {
        return jpaHelper.executeWithTx(em -> {
            return em.find(tClass, id);
        });
    }

    public User getUserByNameEmail() { // TODO add email as login?
        boolean correct = false;
        System.out.println("Enter User Name or Email");
        String userNameEmail = InputUtility.getInputString();
        User user = null;
        while (!correct){
            try {
                String queryName = "select u from User u where u.userName = " + "'" + userNameEmail + "'";
                user = executeTypedQuerySingle(queryName);
                correct = true;
            } catch (NoResultException nre) {
                try {
                    String queryEmail = "select u from User u where u.email = " + "'" + userNameEmail + "'";
                    user = executeTypedQuerySingle(queryEmail);
                    correct = true;
                } catch (NoResultException nre2) {
                    System.out.println("Your name or email can't be found");
                    System.out.println("Enter User Name or Email");
                    userNameEmail = InputUtility.getInputString();
                }
            }
        }
        return user;
    }

    public List<User> findUsers(String name) {
        String fullQuery = "select u from User u where u.userName like " + "'%" + name + "%'";     //TODO change column names + make sql safe!! no stringbuilding
        return executeTypedQuery(fullQuery, User.class);

    }

    public User findUserAssociatedWithEmail(String emailName) {
        String fullQuery = "select u from User u where u.email = " + "'" + emailName + "'";
        return executeTypedQuerySingle(fullQuery, User.class);

    }

    public void addUser(String userName, String password, String email) {
        jpaHelper.execute(eM -> {
            User user = new User(userName, password, email, LocalDateTime.now());
            eM.persist(user);
        });
        System.out.println("User saved");
    }

    public int checkNameAvailability(String name) {
        try {
            String queryName = "select u from User u where u.userName = " + "'" + name + "'";
            User user = executeTypedQuerySingle(queryName);
        } catch (NoResultException nre) {
            return -1;
        }
        return 0;
    }

    public int checkEmailAvailability(String email) {
        try {
            String queryName = "select u from User u where u.email = " + "'" + email + "'";
            User user = executeTypedQuerySingle(queryName);
        } catch (NoResultException nre) {
            return -1;
        }
        return 0;
    }

    public <T> void remove(long id, Class<T> tClass) {
        jpaHelper.execute(eM -> {
            eM.remove(eM.find(tClass, id));
        });
    }
}
