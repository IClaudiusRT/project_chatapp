package utility;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.function.Consumer;
import java.util.function.Function;

public class JpaHelper {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("chatapp");
    }

    public void execute(Consumer<EntityManager> action) {
        execute(em -> {
            action.accept(em);
            return null;
        });
    }

    public <T> T execute(Function<EntityManager, T> action) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try {

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            return action.apply(em);
        } finally {
            if ((tx != null) && (tx.isActive())) {
                tx.commit();
            }
            if (em != null)
                em.close();
        }
    }

    public <T> T executeWithTx(Function<EntityManager, T> action) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            return action.apply(em);
        } finally {
            if ((tx != null) && (tx.isActive())) {
                if (tx.getRollbackOnly()) {
                    tx.rollback();
                } else {
                    tx.commit();
                }
            }
            if (em != null)
                em.close();
        }
    }

}
